### 环境配置

测试前需要修改host文件

127.0.0.1 www.sso.com

127.0.0.1 www.tb.com

127.0.0.1 www.tm.com

redis 地址不需要改直接启动即可

jdk 版本问题 需要修改idea的编译java代码的位置

![image-20220112151916027](https://gitee.com/cold-abyss_admin/my-image-host/raw/master/%20img%20/image-20220112151916027.png)

### 测试需知

8001 作为鉴权服务中心 不能通过外部端口访问，通过跳转访问

测试 tb链接 ：www.tb.com:8002/taobao

测试tm链接：www.tm.com:8003/tmall

账号 : admin

密码：123456

