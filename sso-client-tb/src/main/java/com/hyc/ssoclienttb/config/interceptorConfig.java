package com.hyc.ssoclienttb.config;

import com.hyc.ssoclienttb.interceptor.ssoInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * @projectName: SSO_self
 * @package: com.hyc.ssoclienttb.config
 * @className: interceptorConfig
 * @author: 冷环渊 doomwatcher
 * @description: TODO
 * @date: 2022/1/9 20:34
 * @version: 1.0
 */
@Configuration
public class interceptorConfig extends WebMvcConfigurationSupport {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //创建拦截器类
        HandlerInterceptor interceptor = new ssoInterceptor();

        List<String> patterns = new ArrayList<>();
        //注册拦截器类，添加黑名单(addPathPatterns("/**")),‘/*’只拦截一个层级，'/**'拦截全部
        // 和白名单(excludePathPatterns("List类型参数"))，将不必拦截的路径添加到List列表中
        registry.addInterceptor(interceptor).addPathPatterns("/**").excludePathPatterns(patterns);
    }
}
