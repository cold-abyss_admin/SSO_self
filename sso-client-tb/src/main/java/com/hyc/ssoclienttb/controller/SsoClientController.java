package com.hyc.ssoclienttb.controller;

import com.hyc.ssoclienttb.utils.HttpUitl;
import com.hyc.ssoclienttb.utils.SSOClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

/**
 * @projectName: SSO_self
 * @package: com.hyc.ssoclienttb.controller
 * @className: SsoClientController
 * @author: 冷环渊 doomwatcher
 * @description: TODO
 * @date: 2022/1/9 20:20
 * @version: 1.0
 */
@Controller
public class SsoClientController {
    @RequestMapping("/taobao")
    public String taobao(Model model) {
        model.addAttribute("serverLogouUrl", SSOClientUtil.getClientLogOutUrl());
        System.out.println("成功跳转");
        return "taobao";
    }

    @RequestMapping("/loginOut")
    public String loginout() {
        try {
            String httpURL = SSOClientUtil.getServerLogOutUrl();
            HashMap<String, String> parms = new HashMap<>();
            HttpUitl.sendHttpRequest(httpURL, parms);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:taobao";
    }
}
