package com.hyc.sso_server.controller;

import com.hyc.sso_server.VO.UserAndPassword;
import com.hyc.sso_server.token.userTokenMap;
import com.hyc.sso_server.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Set;
import java.util.UUID;

/**
 * @projectName: SSO_self
 * @package: com.hyc.sso_server.controller
 * @className: loginController
 * @author: 冷环渊 doomwatcher
 * @description: TODO
 * @date: 2022/1/9 12:44
 * @version: 1.0
 */
@Controller
public class loginController {
    @Autowired
    StringRedisTemplate redisTemplate;

    @RequestMapping("/index")
    public String login() {
        return "login";
    }

    UserAndPassword userAndPassword = new UserAndPassword();

    @RequestMapping("/login")
    public String login(String username, String password, String redirectUrl, RedirectAttributes model) {
        System.out.println("loginuser" + "\t" + username + "\t" + "password" + "\t" + password);
        //模拟登录数据库设计
        if ("admin".equals(username) && "123456".equals(password)) {
            //校验成功
            //    1. 创建token
            String token = UUID.randomUUID().toString();
            System.out.println("token创建成功==>" + token);
            //    2. 需要把令牌信息收入到数据库中
            //if (redisTemplate.hasKey("token")) {
            //    redisTemplate.delete("token");
            //}
            redisTemplate.opsForSet().add("token", token);
            userAndPassword.setUsername(username);
            userAndPassword.setPassword(password);
            userTokenMap.tokenmap.put(username, token);
            //    3.重定向到redirecturl
            model.addAttribute("token", token);
            model.addAttribute("username", username);
            System.out.println(redirectUrl);
            return "redirect:" + redirectUrl;
        }
        //如果密码不正确，重新的返回登录页面
        System.out.println("用户密码不正确，重复回到登录页面");
        model.addAttribute("redirectUrl", redirectUrl);
        return "login";
    }

    //检查登录
    @RequestMapping("/checkLogin")
    public String checklogin(String redirectUrl, Model model) {
        String getgenretetoken = userTokenMap.tokenmap.get(userAndPassword.getUsername());
        Set<String> token = redisTemplate.opsForSet().members("token");
        if (!token.contains(getgenretetoken)) {
            //    没有全局会话的话 跳转带统一认证中心的登陆界面
            // 携带转发来的地址 一起转发回去
            model.addAttribute("redirectUrl", redirectUrl);
            return "login";
        } else {
            //   全局会话 取出令牌信息 重定向到 redirecturl
            model.addAttribute("token", getgenretetoken);
            model.addAttribute("username", userAndPassword.getUsername());
            return "redirect:" + redirectUrl;
        }
    }

    @RequestMapping("/islogin")
    @ResponseBody
    public String islogin() {
        String getgenretetoken = userTokenMap.tokenmap.get(userAndPassword.getUsername());
        Set<String> token = redisTemplate.opsForSet().members("token");
        if (!token.contains(getgenretetoken)) {
            return "false";
        } else {
            String islogin = null;
            if (redisTemplate.hasKey(userAndPassword.getUsername())) {
                islogin = redisTemplate.opsForValue().get(userAndPassword.getUsername());
            } else {
                islogin = String.valueOf(RedisUtils.cacheValue(userAndPassword.getUsername(), "true", 30));
            }
            return islogin;
        }
    }

    //校验token 是否是验证中心产生
    @RequestMapping("/verify")
    @ResponseBody
    public String verifytoken(String token) {
        if (redisTemplate.opsForSet().isMember("token", token)) {
            System.out.println("服务端token验证ok===>" + token);
            return "true";
        }
        return "false";
    }

    @RequestMapping("/loginOut")
    @ResponseBody
    public String loginout() {

        RedisUtils.redisTemplate.delete("token");
        RedisUtils.redisTemplate.delete("islogin");
        System.out.println("删除成功");
        return "注销成功";
    }
}
