package com.hyc.sso_server.config;

import com.hyc.sso_server.Listener.keyexpireListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;


@Configuration
public class RedisConfiguration {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    //配置 将spring的 redis链接工厂放入监听容器
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer() {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        return redisMessageListenerContainer;
    }

    //将自定义的监听器放入容器中
    @Bean
    public keyexpireListener keyExpiredListener() {
        return new keyexpireListener(this.redisMessageListenerContainer());
    }
}