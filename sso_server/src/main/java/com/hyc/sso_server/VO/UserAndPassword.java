package com.hyc.sso_server.VO;

import org.springframework.stereotype.Component;

/**
 * @projectName: SSO_self
 * @package: com.hyc.sso_server.VO
 * @className: UserAndPassword
 * @author: 冷环渊 doomwatcher
 * @description: TODO
 * @date: 2022/1/12 0:17
 * @version: 1.0
 */
@Component
public class UserAndPassword {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    @Override
    public String toString() {
        return "UserAndPassword{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public UserAndPassword(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserAndPassword() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password;

}
